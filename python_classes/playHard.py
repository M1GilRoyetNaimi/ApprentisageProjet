from Game import Game
from Player import CPUPlayer, HumanPlayer

# script permettant d’effectuer une partie entre un joueur humain et la machine en mode hard
cpu = CPUPlayer("bot", "hard", 15);
humanP = CPUPlayer("bot2", "medium", 15);

game = Game(15);

game.start(cpu, humanP, True);