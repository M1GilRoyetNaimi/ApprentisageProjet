from Game import Game
from Player import CPUPlayer, HumanPlayer

# script permettant d’effectuer une partie entre un joueur humain et la machine en mode easy
cpu = CPUPlayer("p1", "easy", 15);
humanP = HumanPlayer("p2");

game = Game(15);

game.start(humanP, cpu, True);