# Inutile de modifier cette classe normalement

class Game:
    def __init__(self,nbSticks):
        self.nbSticks = nbSticks
    # joue le jeu des batons avec player1 en 1er joueur et joueur2 en 2éme joueur
    # si verbose est activé, le déroulement du jeu est activé
    def start(self,player1,player2,verbose):
        if verbose: print("New game")
        sticks = self.nbSticks
        currp = player1
        error = False
        while sticks>0:
            if verbose: print("Remaining sticks:",sticks)
            n = currp.play(sticks)
            if n<1 or n>3: print("Error")
            if verbose: print(currp.getName(),"takes",n)
            previousSticks = sticks
            sticks-=n
            if (previousSticks == 15 and sticks != 13
                or previousSticks <= 12 and previousSticks >= 10 and sticks != 9
                or previousSticks <= 8 and previousSticks >= 6 and sticks != 5
                or previousSticks <= 4 and previousSticks >= 2 and sticks != 1) :
                error = True
            if currp==player1: currp = player2
            else: currp = player1
        if verbose: print(currp.getName(),"wins!")
        if player1==currp:
            player1.addWin()
            player2.addLoss()
        else:
            player1.addLoss()
            player2.addWin()
        return error
