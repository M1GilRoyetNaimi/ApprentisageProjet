from Game import Game
from Player import CPUPlayer, HumanPlayer

#50 000 parties en boucles en désactivant le mode "verbose" des parties.
# puis 500 parties permettant de voir le taux d'erreur.
cpu = CPUPlayer("bot1", "hard", 15);
cpu2 = CPUPlayer("bot2", "hard", 15);

game = Game(15);

for i in range(0, 50000):
    game.start(cpu, cpu2, False);

countError = 0
for i in range(0, 500):
    error = game.start(cpu, cpu2, False);
    countError += error

print(str(countError/5) + "% d'erreurs")

