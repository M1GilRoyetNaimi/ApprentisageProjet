from Game import Game
from Player import CPUPlayer, HumanPlayer
#Affichez le nombre de parties gagnées par chaque joueur à la fin de toutes les parties.
cpuEasy1 = CPUPlayer("bot1", "easy", 15);
cpuEasy2 = CPUPlayer("bot2", "easy", 15);
cpuMedium1 = CPUPlayer("bot3", "medium", 15);
cpuMedium2 = CPUPlayer("bot3", "medium", 15);

game = Game(15);


for i in range(0, 800):
    game.start(cpuEasy1, cpuEasy2, False);
print("easy vs easy\n")
print("cpuEasy1 win :" + str(cpuEasy1.getNbWin()))
print("cpuEasy2 win :" + str(cpuEasy2.getNbWin()) + "\n")

cpuEasy1.nbWin = 0
for i in range(0, 800):
    game.start(cpuEasy1, cpuMedium1, False);
print("easy vs medium")
print("cpuEasy win :" + str(cpuEasy1.getNbWin()))
print("cpuMedium win :" + str(cpuMedium1.getNbWin()) + "\n")

cpuEasy1.nbWin = 0
cpuMedium1.nbWin= 0
for i in range(0, 800):
    game.start(cpuMedium1, cpuEasy1, False);
print("medium vs easy")
print("cpuMedium win :" + str(cpuMedium1.getNbWin()))
print("cpuEasy win :" + str(cpuEasy1.getNbWin()) + "\n")


cpuMedium1.nbWin= 0
for i in range(0, 800):
    game.start(cpuMedium1, cpuMedium2, False);
print("medium vs medium")
print("cpuMedium1 win :" + str(cpuMedium1.getNbWin()))
print("cpuMedium2 win :" + str(cpuMedium2.getNbWin()) + "\n")

cpuHardEasy1 = CPUPlayer("bot3", "hard", 15);
cpuHardEasy2 = CPUPlayer("bot3", "hard", 15);
cpuHardMedium1 = CPUPlayer("bot3", "hard", 15);
cpuHardMedium2 = CPUPlayer("bot3", "hard", 15);
cpuHardHard1 = CPUPlayer("bot3", "hard", 15);
cpuHardHard2 = CPUPlayer("bot3", "hard", 15);

cpuEasy1.nbWin= 0
for i in range(0, 800):
    game.start(cpuEasy1, cpuHardEasy1, False);
print("easy vs hard")
print("cpuEasy win :" + str(cpuEasy1.getNbWin()))
print("cpuHard win :" + str(cpuHardEasy1.getNbWin()) + "\n")

cpuEasy1.nbWin= 0
for i in range(0, 800):
    game.start(cpuHardEasy2, cpuEasy1, False);
print("hard vs easy")
print("cpuHard win :" + str(cpuHardEasy2.getNbWin()))
print("cpuEasy win :" + str(cpuEasy1.getNbWin()) + "\n")

cpuMedium1.nbWin= 0
for i in range(0, 800):
    game.start(cpuMedium1, cpuHardMedium1, False);
print("medium vs hard")
print("cpuMedium win :" + str(cpuMedium1.getNbWin()))
print("cpuHard win :" + str(cpuHardMedium1.getNbWin()) + "\n")

cpuMedium1.nbWin= 0
for i in range(0, 800):
    game.start(cpuHardMedium2, cpuMedium1, False);
print("hard vs medium")
print("cpuHard win :" + str(cpuHardMedium2.getNbWin()))
print("cpuMedium win :" + str(cpuMedium1.getNbWin()) + "\n")

for i in range(0, 800):
    game.start(cpuHardHard1, cpuHardHard2, False);
print("hard vs hard")
print("cpuHard1 win :" + str(cpuHardHard1.getNbWin()))
print("cpuHard2 win :" + str(cpuHardHard2.getNbWin()) + "\n")