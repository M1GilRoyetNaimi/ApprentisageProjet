from Game import Game
from Player import CPUPlayer, HumanPlayer

#script correspondant au jeu "final"

correct = False
nom = "";
nom = input('Quel est votre nom ?\n')
mode = "";
while not correct:
    mode = input('Mode (easy, medium, hard)?\n')
    try:
        if mode == "hard" or mode == "medium" or mode == "easy":
            correct=True
    except: pass

correct = False
position = "";
while not correct:
    position = input('jouer en 1er ou 2éme(1, 2)?\n')
    try:
        if position == "1" or position == "2":
            correct=True
    except: pass
humanP = HumanPlayer(nom);
cpu = CPUPlayer("bot", mode, 15);


game = Game(15);

if (mode == "hard"):
    # init hard IA
    cpu.loadNetwork()


if (position == 1):
    game.start(humanP, cpu, True);
else :
    game.start(cpu, humanP, True);

