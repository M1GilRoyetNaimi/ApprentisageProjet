from Game import Game
from Player import CPUPlayer, HumanPlayer

#800 parties en boucles en désactivant le mode "verbose" des parties.
cpu = CPUPlayer("bot1", "hard", 15);
cpu2 = CPUPlayer("bot2", "hard", 15);

game = Game(15);

for i in range(0, 800):
    game.start(cpu, cpu2, False);

print("cpu win :" + str(cpu.getNbWin()) + "\n")
print("cpu2 win :" + str(cpu2.getNbWin()) + "\n")
print("reseau neuron cpu\n")
cpu.getNeuronNetwork().printAllConnections()

print("reseau neuron cpu2\n")

cpu2.getNeuronNetwork().printAllConnections()