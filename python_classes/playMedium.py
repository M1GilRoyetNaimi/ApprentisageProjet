from Game import Game
from Player import CPUPlayer, HumanPlayer

# script permettant d’effectuer une partie entre un joueur humain et la machine en mode medium
cpu = CPUPlayer("bot", "medium", 15);
humanP = HumanPlayer("p1");

game = Game(15);

game.start(humanP, cpu, True);